import PyTango
import time

cyberstar = PyTango.DeviceProxy("d13-1-cx1/dt/cyberstar.1")
cyberstar.set_timeout_millis(3000)

lower  = cyberstar.read_attribute("scaLowerThreshold")
upper = cyberstar.read_attribute("scaUpperThreshold")
width = cyberstar.read_attribute("windowWidth")
center = cyberstar.read_attribute("windowCenterPosition")


center.value=1
#cyberstar.write_attribute(center)
width.value = 1
cyberstar.write_attribute(width)
error_lower=0
error_upper=0
for i in range(1000):
	print "step %i" % i	
	for j in (9., 5., 1.):
		center.value = j
		cyberstar.write_attribute(center)
		print "-->" , j	
		print cyberstar.read_attribute("scaLowerThreshold").value
		print cyberstar.read_attribute("scaUpperThreshold").value
		
		
		lower_read= cyberstar.read_attribute("scaLowerThreshold").value
                upper_read= cyberstar.read_attribute("scaUpperThreshold").value
		
		if lower_read != (j-width.value/2.) :
			print "error setting lower %f instead of %f %d" % (lower_read, (j-width.value/2.), error_lower) 
                      	error_lower=error_lower+1
		if upper_read != (j+width.value/2.) :
			print "error setting upper %f instead of %f %d" % (upper_read, (j+width.value/2.), error_upper) 
			#print "error setting upper", error_lower 
               		error_upper=error_upper+1


"""


upper.value = 10
cyberstar.write_attribute(upper)
error_lower=0

for i in range(1000):
	for j in (9, 5, 1):
		lower.value = j
		cyberstar.write_attribute(lower)	
		print cyberstar.read_attribute("scaLowerThreshold").value
		lower_read= cyberstar.read_attribute("scaLowerThreshold").value
                if lower_read != j :
			print "error setting lower", error_lower 
                        error_lower=error_lower+1
			

lower.value = 0.5
cyberstar.write_attribute(lower)

for i in range(1000):
	print i
	debut = time.time()
	for j in (9, 5, 1):
		upper.value = j
		cyberstar.write_attribute(upper)	
		print cyberstar.read_attribute("scaUpperThreshold").value
	fin = time.time()
	print "t = %f" % (fin - debut)

"""
